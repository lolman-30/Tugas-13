<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TableController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/table', [TableController::class, 'showTable']);
Route::get('/data-table', [TableController::class, 'showDataTables']);

Route::get('/master', function () {
    return view('layouts.master');
});
